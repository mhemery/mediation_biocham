"""
The traditionnal lotka volterra model.
Oscillations are not guaranty due to the small population size.
"""
import animation as ani

population = [('r', 10), ('g', 20)]

reactions = ["g =0.1=> g+g",
             "g+r =0.6=> r",
             "g+r =0.2=> r+r",
             "g =0.03=> o",
             "r =0.03=> o"]

ani.animation(population, reactions, my_seed=1)
