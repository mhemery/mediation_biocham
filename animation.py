"""
A rather naive python script to generate animation of 2D molecular dynamics
with bi-chemical like reactions in order to ease the mediation of Biocham.

Reactions involved species named after the color code of matplotlib: "rgbcmyk"
The empty set is denoted by 'o'

Are allowed reactions with no, one or two reactants and one more product.

The rate is given in the middle of the reaction arrow.
Examples:
o =2=> b
b+g =0.25=> k
r =1=> r+r

Coder: M. Hemery
"""
from math import sqrt
from numpy.random import uniform, normal, poisson, random, shuffle, seed
from functools import partial
from matplotlib.pyplot import show, figure
from matplotlib.animation import FuncAnimation
import re

# Global parameters
DIST_INT = 0.1  # Defines the minimal distance for two molecules interactions
SIGMA = 0.02  # Standard deviation of motion for each time step
MAXPOP = 100  # Cut off for the population to avoid slow down


def dist(Ind1: list, Ind2: list) -> float:
    """Compute the distance between two molecules"""
    return sqrt((Ind1[0] - Ind2[0])**2 + (Ind1[1] - Ind2[1])**2)


class Reaction():
    """A reaction class to store the information of each reaction"""

    def __init__(self, my_description):
        """Convert a string representation of reaction to a class instance"""
        self.description = my_description
        search = re.search('(.*)=(.*)=>(.*)', my_description)
        Reac, Rate, Prod = search.groups()
        Reac = Reac.strip()
        Rate = Rate.strip()
        Prod = Prod.strip()
        self.rate = float(Rate)
        if Reac == 'o':
            self.type = 0
            self.to_create = Prod
            self.to_delete = []
        elif '+' in Reac:
            self.type = 2
            self.to_delete = Reac.split('+')
            self.reactant = set(self.to_delete)
            self.to_create = Prod.split('+')
            if 'o' in self.to_create:
                self.to_create.remove('o')
            if self.to_delete[1] in self.to_create:
                tempo = self.to_delete.pop(1)
                self.to_create.remove(tempo)
            if self.to_delete[0] in self.to_create:
                tempo = self.to_delete.pop(0)
                self.to_create.remove(tempo)
        else:
            self.type = 1
            self.reactant = Reac
            self.to_create = []
            if Reac not in Prod:
                self.to_delete = True
            else:
                self.to_delete = False
            self.to_create = Prod.split('+')
            if Reac in self.to_create:
                self.to_create.remove(Reac)
            if 'o' in self.to_create:
                self.to_create.remove('o')

    def __call__(self, *args):
        """Given args molecules determine if the reaction triggers or not"""
        if len(args) != self.type:
            return False
        if self.type == 0:
            return poisson(self.rate)
        elif self.type == 1 and args[0][2] == self.reactant:
            return random() < self.rate
        elif self.type == 2 and {args[0][2], args[1][2]} == self.reactant:
            if dist(args[0], args[1]) < DIST_INT:
                return random() < self.rate
            else:
                return False


def generate_random_population(list_type: list):
    """Generate the random population, input is a list of tuples the form (type, int)"""
    population = []
    for type, n_int in list_type:
        for i0 in range(n_int):
            x, y = uniform(0, 1, 2)
            population.append([x, y, type])
    return population


def plot_population(ax, population: list):
    """plot the population in a 2D space"""
    if population:
        x, y, c = zip(*population)
        ax.clear()
        ax.set(xlim=[0, 1], ylim=[0, 1])
        scat = ax.scatter(x, y, s=50, c=c)
    else:
        scat = ax.scatter([], [], [])
    return scat


def plot_count(ax, frame: int, population: list):
    """Plot the number of each molecules at current frame"""
    pop_type = [mol[2] for mol in population]
    for type in "rgbcmyk":
        count = pop_type.count(type)
        if count:
            plot = ax.plot([frame], [count], 'o'+type)
    return plot


def update(ax1, ax2, population: list, reaction_list: list, frame: int):
    """update both the population and the plots"""
    if frame == 100:
        return None
    motion(population)
    population = apply_reactions(population, reaction_list)
    scat = plot_population(ax1, population)
    plot = plot_count(ax2, frame, population)
    return scat, plot


def motion(population: list, sigma: float = SIGMA):
    """Move each molecules of the population"""
    for mol in population:
        dx, dy = normal(0, sigma, 2)
        mol[0] = (mol[0]+dx) % 1
        mol[1] = (mol[1]+dy) % 1


def apply_reactions(population, reaction_list):
    new_mol = []
    to_delete = []
    # bimolecular_reactions
    for mol1 in population:
        for mol2 in population:
            if mol1[0] < mol2[0]:
                continue
            for reaction in reaction_list:
                if reaction(mol1, mol2):
                    if len(reaction.to_delete) == 2:
                        to_delete.append(mol1)
                        to_delete.append(mol2)
                    elif len(reaction.to_delete) == 1:
                        if mol1[2] in reaction.to_delete:
                            to_delete.append(mol1)
                        else:
                            to_delete.append(mol2)
                    if reaction.to_create:
                        xm = (mol1[0]+mol2[0])/2
                        ym = (mol1[1]+mol2[1])/2
                    for type in reaction.to_create:
                        new_mol.append([xm, ym, type])
    # monomolecular_reactions
    for mol in population:
        for reaction in reaction_list:
            if reaction(mol):
                if reaction.to_delete:
                    to_delete.append(mol)
                for sp in reaction.to_create:
                    new_mol.append([mol[0], mol[1], sp])
    # basal_reactions
    for reaction in reaction_list:
        N = reaction()
        if N:
            for i0 in range(N):
                x, y = uniform(0, 1, 2)
                new_mol.append([x, y, reaction.to_create])
    # processing the transformation to update the population
    for el in to_delete:
        try:  # due to 2 reactions, some elements may be deleted twice
            population.remove(el)
        except ValueError:
            pass
    population += new_mol

    if len(population) > MAXPOP:
        shuffle(population)
        return population[:MAXPOP]
    else:
        return population


def animation(population: list,
              reaction_lists: list,
              total_frames: int = 100,
              my_seed: int = -1,
              save_name: str = None):
    """Generate a gif of the described system"""
    if my_seed > 0:
        seed(my_seed)
    fig = figure(figsize=(16, 8))
    ax1 = fig.add_subplot(1, 2, 1)
    ax2 = fig.add_subplot(1, 2, 2)
    ax2.plot([0, total_frames], [0, 0])
    ax1.set(xlim=[0, 1], ylim=[0, 1])
    if population == []:
        population = [('r', 5), ('g', 5)]
    pop = generate_random_population(population)
    formated_reaction = [Reaction(r) for r in reaction_lists]
    up = partial(update, ax1, ax2, pop, formated_reaction)
    ani = FuncAnimation(fig=fig, func=up,
                        frames=total_frames, interval=50, repeat=False)
    if save_name:
        ani.save(save_name)
    else:
        show()


def test_reactions():
    for string in ["o =1=> b",
                   "o=0.5=>a+b",
                   "b=0.1=>o",
                   "a=0.1=>a+b",
                   "a+b=1=>o",
                   "a+a=1=>a+b"]:
        reaction = Reaction(string)
        print(string, reaction.to_create, reaction.to_delete)


if __name__ == "__main__":
    test_reactions()
    clock = ["r+g=0.5=>r+b",
             "b+r=0.5=>b+k",
             "k+b=0.5=>k+g",
             "g+k=0.5=>g+r"]
    clock2 = ["r=0.1=>r+b",
              "b=0.1=>b+k",
              "k=0.1=>k+g",
              "g=0.1=>g+r",
              "r+k=1=>o",
              "g+b=1=>o"]
    # animation([('r', 10)], clock2)
    # animation([('g', 10), ('r', 12)], ["r+g=1=>b"])
    # animation([], ["r=0.1=>r+r", "r=0.1=>o", "g=0.05=>o"])
    # animation([], ["o=0.2=>g", "g=0.02=>r+r", "r=0.1=>o"])
