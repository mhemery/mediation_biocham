"""
Implementation of our own model of cosine as given by the CRN compiler.
"""
import animation as ani

population = [('r', 20)]

reactions = ["r=0.05=>r+b",
             "b=0.05=>b+k",
             "k=0.05=>k+g",
             "g=0.05=>g+r",
             "r+k=1=>o",
             "g+b=1=>o"]

ani.animation(population, reactions, total_frames=200)
