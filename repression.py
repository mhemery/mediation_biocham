"""
A simple model of repression, 'r' binds to 'g' to form the inactivator complex
'k' that forbid 'b' to produce 'c'.
Try playing with the initial concentration of 'r' and 'g' to see the effects.
"""
import animation as ani

initial_population = [("r", 10), ("g", 0), ("b", 10)]

reaction_list = ["r+g =1=> k",
                 "k+b =1=> k+y",
                 "b =0.1=> b+c",
                 "c =0.05=> o"]

ani.animation(initial_population, reaction_list)
